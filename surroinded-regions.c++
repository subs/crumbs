/* 
 *  Kind of nice way to do a dfs over a grid with an int[4] of directions. 
 */

class Solution {
public:

    bool doDfs (vector<vector<char>>& board, int i, int j, char c) {
        
      if (board[i][j]==c) return true;
      if (board[i][j]=='x') return true;    
        
      int[4][2] directions = { {1,0}, {-1,0}, {0,1}, {0,-1} };
      
      board[i][j] = c;
      
      for (int d=0; d<4;d++ ) {
          int[2] dir = directions[d];
          int i2 = i + dir[0];
          int j2 = j + dir[1];
          
          if (i2 < 0 || i2 >= board.size()) continue;
          if (j2 < 0 || j2 >= board[0].size()) continue;
          
          doDfs(board,i2,j2,c);
          
      }
      
      return true;
        
        
    }


    void solve(vector<vector<char>>& board) {
        
        vector<vector<char>> visited = board;
        
        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j< board[0].size();  j += (i==0 || i==board.size() - 1) ? 1 : board[0].size()-1 ) {
                
                if (board[i][j]=='o') {
                    doDfs(visited,i,j,'v');
                    
                }
                
        
            }
        }    
        
        
        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j< board[0].size();  j ++ ) {
                
                if (visited[i][j]=='o') {
                    doDfs(board,i,j,'x');
                }
            }
        
        
    }
};
