/*
 * Im not really sure what the problem was, but the point is
 *  that in a dynamic programming type scenario where the next
 *  level (l) of solution only depends on the one before it, and you
 *  need to save memory, you can avoid copying around the last
 *  used array by using a 2xN matrix and indexing using l mod 2.
 *  This way you always reuse the l mod 2 solution slots with 
 *  a value depending on your previous solution in the (l-1) mod 2
 *  slots. 
 */

class Solution {
 public:
  bool checkSubarraySum(vector<int>& nums, int k) {
    int N=nums.size();

    //avoid memory too big, with dp rollowver
    vector<vector<int> > sums(2,vector<int>(N+1,0));

    for (int l=1; l<=N; l++) {
      for (int p=0; p<N-l+1; p++) {

        if (l==1)
          sums[l%2][p] = nums[p];
        else {
          // Check sums with length longer than 2.

          sums[l%2][p] = sums[(l-1)%2][p] + nums[p+l-1];
          
          if (k!=0)  {
            if (sums[l%2][p]%k == 0)
              return true;
          }  else  {  // k==0 special case
            if (sums[l%2][p] == 0)
              return true;
          }

        }
      }
    }
    return false;
  }

};
